package com.illuzor.fluxapp.base.utils

import android.os.Handler
import android.os.Looper

fun callWithDelay(delay: Long = 1000, action: () -> Unit) {
    // Only for demo. Do not create threads in your applications!
    Thread {
        Thread.sleep(delay)
        Handler(Looper.getMainLooper()).post {
            action()
        }
    }.start()
}
