package com.illuzor.fluxapp.base.model

data class ItemModel(val id: Int, val title: String, val selected: Boolean)
