package com.illuzor.fluxapp.base

sealed class GlobalAction : Action {
    object UpdateAll : GlobalAction()
}
