package com.illuzor.fluxapp.base

import android.util.Log
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Dispatcher @Inject constructor() {

    private val listeners = mutableListOf<(Action) -> Unit>()

    fun postAction(action: Action) {
        Log.i("Dispatcher", "Action ${action::class.java.simpleName} posted")
        listeners.forEach { it.invoke(action) }
    }

    fun subscribe(callback: (Action) -> Unit) = listeners.add(callback)

    fun unsubscribe(callback: (Action) -> Unit) = listeners.remove(callback)
}
