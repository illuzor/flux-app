package com.illuzor.fluxapp.base.storage

import com.illuzor.fluxapp.base.model.ItemModel
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DemoRepository @Inject constructor() {

    private companion object {
        const val LIST_SIZE = 8
    }

    @OptIn(ExperimentalStdlibApi::class)
    private var items = buildList {
        repeat(LIST_SIZE) {
            add(ItemModel(it, "Item $it", false))
        }
    }

    fun updateItem(id: Int, isSelected: Boolean) {
        items = items.map { if (it.id == id) it.copy(selected = isSelected) else it }
    }

    fun getItem(id: Int): ItemModel = items.find { it.id == id }!!

    fun getItems(): List<ItemModel> = items
}
