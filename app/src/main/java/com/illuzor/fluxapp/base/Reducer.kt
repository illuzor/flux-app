package com.illuzor.fluxapp.base

interface Reducer<A : Action, S> {
    operator fun invoke(state: S, action: A): S
}
