package com.illuzor.fluxapp.screens.item

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.illuzor.fluxapp.base.Action
import com.illuzor.fluxapp.base.Dispatcher
import com.illuzor.fluxapp.base.GlobalAction
import com.illuzor.fluxapp.base.utils.callWithDelay
import com.illuzor.fluxapp.base.storage.DemoRepository
import com.illuzor.fluxapp.screens.item.data.ItemAction
import com.illuzor.fluxapp.screens.item.data.ItemReducer
import com.illuzor.fluxapp.screens.item.data.ItemState
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ItemViewModel @Inject constructor(
    private val repository: DemoRepository,
    private val dispatcher: Dispatcher,
    private val reducer: ItemReducer
) : ViewModel() {

    private val actionListener: (Action) -> Unit = { action ->
        if (action is ItemAction) {
            performAction(action)
        }
    }

    private var itemId: Int = -1

    private val stateInternal: MutableLiveData<ItemState> = MutableLiveData(ItemState.initialState)
    val state: LiveData<ItemState> by ::stateInternal

    init {
        dispatcher.subscribe(actionListener)
    }

    fun requestItem(id: Int) {
        if (itemId >= 0) {
            return
        }

        itemId = id
        dispatcher.postAction(ItemAction.Progress)
        callWithDelay {
            dispatcher.postAction(ItemAction.ShowContent(repository.getItem(itemId)))
        }
    }

    fun updateItem(checked: Boolean) {
        repository.updateItem(itemId, checked)
        dispatcher.postAction(GlobalAction.UpdateAll)
        callWithDelay(0) {
            dispatcher.postAction(ItemAction.ShowContent(repository.getItem(itemId)))
        }
    }

    override fun onCleared() {
        dispatcher.unsubscribe(actionListener)
    }

    private fun performAction(action: ItemAction) {
        stateInternal.value = reducer(state.value!!, action)
    }
}
