package com.illuzor.fluxapp.screens.list.adapter

import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.illuzor.fluxapp.base.model.ItemModel
import com.illuzor.fluxapp.databinding.ItemBinding

class ItemViewHolder(val viewBinding: ItemBinding) : ViewHolder(viewBinding.root) {

    fun bind(item: ItemModel) {
        viewBinding.tvTitle.text = item.title
        viewBinding.stitchEnabled.isChecked = item.selected
    }
}
