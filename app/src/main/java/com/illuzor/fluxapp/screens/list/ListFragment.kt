package com.illuzor.fluxapp.screens.list

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.fragment.app.viewModels
import by.kirich1409.viewbindingdelegate.viewBinding
import com.illuzor.fluxapp.R
import com.illuzor.fluxapp.base.model.ItemModel
import com.illuzor.fluxapp.databinding.FragmentListBinding
import com.illuzor.fluxapp.screens.item.ItemFragment
import com.illuzor.fluxapp.screens.list.adapter.ItemsAdapter
import com.illuzor.fluxapp.screens.list.data.ListState
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ListFragment : Fragment(R.layout.fragment_list) {

    private val viewModel: ListViewModel by viewModels()
    private val viewBinding by viewBinding(FragmentListBinding::bind)
    private val adapter = ItemsAdapter()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupAdapter()
        setupClickListeners()

        viewModel.state.observe(viewLifecycleOwner) { state ->
            when (state) {
                is ListState.InitialProgress -> showInitialProgress()
                is ListState.UpdatingProgress -> showUpdatingProgress()
                is ListState.Content.Error -> showError(state.message)
                is ListState.Content.Empty -> showEmpty()
                is ListState.Content.Items -> showItems(state.items)
                else -> Unit
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewBinding.rvList.adapter = null
    }

    private fun setupAdapter() {
        adapter.setItemSelectedListener { id, isChecked ->
            viewModel.itemChecked(id, isChecked)
        }

        // TODO replace with router in viewModel
        adapter.setItemOpenListener { id ->
            parentFragmentManager.commit {
                add(R.id.fragment_container, ItemFragment.createInstance(id))
                addToBackStack(null)
            }
        }

        viewBinding.rvList.adapter = adapter
    }

    private fun setupClickListeners() {
        viewBinding.btnReload.setOnClickListener {
            viewModel.reload()
        }

        viewBinding.btnEmpty.setOnClickListener {
            viewModel.reloadEmpty()
        }

        viewBinding.btnError.setOnClickListener {
            viewModel.reloadError()
        }
    }

    private fun showInitialProgress() {
        viewBinding.apply {
            tvError.isVisible = false
            rvList.isVisible = false
            pbLoading.isVisible = true
        }
    }

    private fun showUpdatingProgress() =
        Toast.makeText(requireContext(), "Updating...", Toast.LENGTH_SHORT).show()

    private fun showEmpty() = showError("Nothing to show")

    private fun showError(message: String) {
        viewBinding.apply {
            tvError.text = message
            tvError.isVisible = true
            rvList.isVisible = false
            pbLoading.isVisible = false
        }
    }

    private fun showItems(items: List<ItemModel>) {
        viewBinding.apply {
            tvError.isVisible = false
            rvList.isVisible = true
            pbLoading.isVisible = false
        }

        adapter.submitList(items)
    }
}
