package com.illuzor.fluxapp.screens.list.data

import com.illuzor.fluxapp.base.model.ItemModel

sealed class ListState {
    object Init : ListState()
    object InitialProgress : ListState()
    object UpdatingProgress : ListState()

    sealed class Content : ListState() {
        class Error(val message: String) : Content()
        object Empty : Content()
        data class Items(val items: List<ItemModel>) : Content()
    }

    companion object {
        val initialState: ListState = Init
    }
}
