package com.illuzor.fluxapp.screens.item

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import by.kirich1409.viewbindingdelegate.viewBinding
import com.illuzor.fluxapp.R
import com.illuzor.fluxapp.base.model.ItemModel
import com.illuzor.fluxapp.databinding.FragmentItemBinding
import com.illuzor.fluxapp.screens.item.data.ItemState
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ItemFragment : Fragment(R.layout.fragment_item) {

    companion object {

        private const val ITEM_ID_KEY = "item_id_key"

        fun createInstance(itemId: Int): Fragment = ItemFragment().apply {
            arguments = bundleOf(ITEM_ID_KEY to itemId)
        }
    }

    private val viewModel: ItemViewModel by viewModels()
    private val viewBinding by viewBinding(FragmentItemBinding::bind)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewBinding.stitchEnabled.setOnCheckedChangeListener { _, isChecked ->
            viewModel.updateItem(isChecked)
        }

        viewModel.requestItem(requireArguments().getInt(ITEM_ID_KEY))

        viewModel.state.observe(viewLifecycleOwner) { state ->
            when (state) {
                is ItemState.Content -> showItem(state.item)
                ItemState.Progress -> showProgress()
                else -> Unit
            }
        }
    }

    private fun showItem(item: ItemModel) {
        viewBinding.pbLoading.isVisible = false
        viewBinding.tvTitle.isVisible = true
        viewBinding.stitchEnabled.isVisible = true

        viewBinding.tvTitle.text = item.title
        viewBinding.stitchEnabled.isChecked = item.selected
    }

    private fun showProgress() {
        viewBinding.pbLoading.isVisible = true
        viewBinding.tvTitle.isVisible = false
        viewBinding.stitchEnabled.isVisible = false
    }
}
