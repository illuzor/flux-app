package com.illuzor.fluxapp.screens.list.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import com.illuzor.fluxapp.base.model.ItemModel
import com.illuzor.fluxapp.databinding.ItemBinding

class ItemsAdapter : ListAdapter<ItemModel, ItemViewHolder>(ItemsDiffCallback()) {

    private var itemSelectionListener: ((id: Int, isChecked: Boolean) -> Unit)? = null
    private var itemOpenListener: ((id: Int) -> Unit)? = null


    fun setItemSelectedListener(listener: (id: Int, isChecked: Boolean) -> Unit) {
        itemSelectionListener = listener
    }

    fun setItemOpenListener(listener: (id: Int) -> Unit) {
        itemOpenListener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val viewBinding = ItemBinding.inflate(inflater)
        return ItemViewHolder(viewBinding)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item)

        holder.viewBinding.stitchEnabled.setOnCheckedChangeListener { _, isChecked ->
            itemSelectionListener?.invoke(item.id, isChecked)
        }

        holder.viewBinding.btnOpen.setOnClickListener {
            itemOpenListener?.invoke(item.id)
        }
    }
}
