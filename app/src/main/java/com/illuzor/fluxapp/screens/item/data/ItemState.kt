package com.illuzor.fluxapp.screens.item.data

import com.illuzor.fluxapp.base.model.ItemModel

sealed class ItemState {

    object Init : ItemState()
    object Progress : ItemState()
    data class Content(val item: ItemModel) : ItemState()

    companion object {
        val initialState: ItemState = Init
    }
}
