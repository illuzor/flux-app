package com.illuzor.fluxapp.screens.list.data

import com.illuzor.fluxapp.base.Reducer
import javax.inject.Inject

class ListReducer @Inject constructor() : Reducer<ListAction, ListState> {

    override fun invoke(state: ListState, action: ListAction): ListState =
        when (action) {
            ListAction.InitialLoad -> ListState.InitialProgress
            ListAction.UpdatingLoad -> ListState.UpdatingProgress
            is ListAction.ShowContent ->
                when {
                    action.errorMessage.isNotEmpty() -> ListState.Content.Error(action.errorMessage)
                    action.data.isEmpty() -> ListState.Content.Empty
                    action.data.isNotEmpty() -> ListState.Content.Items(action.data)
                    else -> ListState.Content.Error("Something wrong")
                }
        }
}
