package com.illuzor.fluxapp.screens.item.data

import com.illuzor.fluxapp.base.Reducer
import javax.inject.Inject

class ItemReducer @Inject constructor() : Reducer<ItemAction, ItemState> {

    override fun invoke(state: ItemState, action: ItemAction): ItemState =
        when (action) {
            ItemAction.Progress -> ItemState.Progress
            is ItemAction.ShowContent -> ItemState.Content(action.item)
        }
}
