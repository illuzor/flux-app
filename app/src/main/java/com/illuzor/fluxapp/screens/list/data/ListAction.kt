package com.illuzor.fluxapp.screens.list.data

import com.illuzor.fluxapp.base.Action
import com.illuzor.fluxapp.base.model.ItemModel

sealed class ListAction : Action {
    object InitialLoad : ListAction()
    object UpdatingLoad : ListAction()

    data class ShowContent(
        val data: List<ItemModel> = emptyList(),
        val errorMessage: String = ""
    ) : ListAction()
}
