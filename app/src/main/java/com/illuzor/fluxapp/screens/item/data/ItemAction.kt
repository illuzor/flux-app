package com.illuzor.fluxapp.screens.item.data

import com.illuzor.fluxapp.base.Action
import com.illuzor.fluxapp.base.model.ItemModel

sealed class ItemAction : Action {
    object Progress : ItemAction()
    data class ShowContent(val item: ItemModel) : ItemAction()
}
