package com.illuzor.fluxapp.screens.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.illuzor.fluxapp.base.Action
import com.illuzor.fluxapp.base.Dispatcher
import com.illuzor.fluxapp.base.GlobalAction
import com.illuzor.fluxapp.base.utils.callWithDelay
import com.illuzor.fluxapp.base.model.ItemModel
import com.illuzor.fluxapp.base.storage.DemoRepository
import com.illuzor.fluxapp.screens.list.data.ListAction
import com.illuzor.fluxapp.screens.list.data.ListReducer
import com.illuzor.fluxapp.screens.list.data.ListState
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ListViewModel @Inject constructor(
    private val repository: DemoRepository,
    private val dispatcher: Dispatcher,
    private val reducer: ListReducer,
) : ViewModel() {

    private val actionListener: (Action) -> Unit = { action ->
        prePerformAction(action)
    }

    private val stateInternal: MutableLiveData<ListState> = MutableLiveData(ListState.initialState)
    val state: LiveData<ListState> by ::stateInternal

    init {
        dispatcher.subscribe(actionListener)

        dispatcher.postAction(ListAction.InitialLoad)
        loadData(2000) {
            dispatcher.postAction(ListAction.ShowContent(it))
        }
    }

    fun itemChecked(id: Int, isChecked: Boolean) {
        repository.updateItem(id, isChecked)
        loadData(0) {
            dispatcher.postAction(ListAction.ShowContent(it))
        }
    }

    fun reload() {
        dispatcher.postAction(ListAction.UpdatingLoad) // TODO replace with single emmit LiveData
        loadData {
            dispatcher.postAction(ListAction.ShowContent(it))
        }
    }

    fun reloadEmpty() {
        dispatcher.postAction(ListAction.UpdatingLoad) // TODO replace with single emmit LiveData
        callWithDelay {
            dispatcher.postAction(ListAction.ShowContent(emptyList()))
        }
    }

    fun reloadError() {
        dispatcher.postAction(ListAction.UpdatingLoad) // TODO replace with single emmit LiveData
        callWithDelay {
            dispatcher.postAction(ListAction.ShowContent(errorMessage = "ERROR: Unable to load data"))
        }
    }

    override fun onCleared() {
        dispatcher.unsubscribe(actionListener)
    }

    private fun prePerformAction(action: Action) {
        when {
            action.shouldSkip() -> Unit
            action is ListAction -> performAction(action)
            action is GlobalAction.UpdateAll -> {
                loadData(0) {
                    dispatcher.postAction(ListAction.ShowContent(it))
                }
            }
        }
    }

    private fun performAction(action: ListAction) {
        stateInternal.value = reducer(state.value!!, action)
    }

    /** Simulation of async loading */
    private fun loadData(delay: Long = 1000, listener: (List<ItemModel>) -> Unit) {
        callWithDelay(delay) {
            listener(repository.getItems())
        }
    }

    private fun Action.shouldSkip(): Boolean =
        this !is ListAction && this !is GlobalAction.UpdateAll
}
