package com.illuzor.fluxapp.screens.list.adapter

import androidx.recyclerview.widget.DiffUtil
import com.illuzor.fluxapp.base.model.ItemModel

class ItemsDiffCallback : DiffUtil.ItemCallback<ItemModel>() {

    override fun areItemsTheSame(old: ItemModel, new: ItemModel) = old.id == new.id

    override fun areContentsTheSame(old: ItemModel, new: ItemModel) =
        old.title == new.title && old.selected == new.selected
}
